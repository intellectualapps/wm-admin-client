$(document).ready(function() {
    $('#loginForm').on("submit", function(event) {
        event.preventDefault();
        var email = $('#email').val();
        var password = $('#password').val();
        $('body').prepend('<div id="loader"></div>');
        $('#myDiv').attr("hidden", true);
        loginUser(email, password);
    });
});

function loginUser(email, password) {
    $.ajax({
        url: 'https://general-purpose-165811.appspot.com/api/v1/user/authenticate',
        method: 'POST',
        data: "email="+email+"&password="+password
    })
    .done(function(response) {
        var responseObj = JSON.parse(response);
        if(responseObj.status) {
            $('#loginForm').prepend('<div class="alert alert-warning alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert"aria-label="Close"><span aria-hidden="true">&times;</span></button><strong>Oops!</strong> '+responseObj.message+'</div>');
            $('#myDiv').attr("hidden", false);
            $("#loader").remove();
        } else {
            setCookie("pentor_email", responseObj.email, 1);
            setCookie("pentor_name", responseObj.firstName+' '+responseObj.lastName, 1);
            setCookie("pentor_authToken", responseObj.authToken, 1);
            setTimeout(function() { window.location = '/user-profile' }, 2000);
        }
    })
    .fail(function(response) {
        var responseObj = JSON.parse(response);
        $('#loginForm').prepend('<div class="alert alert-warning alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert"aria-label="Close"><span aria-hidden="true">&times;</span></button><strong>Warning!</strong> '+responseObj.message+'</div>');
        $('#myDiv').attr("hidden", false);
        $("#loader").remove();
    });
    setTimeout(function() { $(".alert").remove(); }, 10000);
}
